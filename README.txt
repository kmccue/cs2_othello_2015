The idea behind my AI was to prioritize certain board positions and make
game decisions based on this priority list, rather than on the potential 
gain each move would make. I chose the priority of the positions based on
my own othello strategy, which does not focus on the number of pieces 
gained in the short term, but rather the strategic value of those positions
in the long run, as, in my experience, a game of othello is generally 
determined by the last few moves, relating to the number of strategically 
important pieces one has captured throughout the game. Thus, I did not
implement the minimax algorithm, as I feel it a somewhat misguided way
to approach such a strategically complex game. This is not to say that my
AI's algorithm is perfect, but it does beat BetterPlayer in the two 
deterministic games that occur between them, and also beats simplePlayer
and constantTimePlayer a vast majority of the time (about 4/5ths of the 
time against simplePlayer and more than 9/10ths of the time against 
constantTimePlayer). 
