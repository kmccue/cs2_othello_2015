#include "player.h"

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;
    
    // Allocate memory for the board, and the sides
    board = new Board();
    mySide = new Side;
    otherSide = new Side; 
    
    // Save the sides, both AI's and opponent's. 
    *mySide = side; 
    if (*mySide == WHITE) *otherSide = BLACK;
    else *otherSide = WHITE;
}

/*
 * Destructor for the player.
 */
Player::~Player() {
    delete board;
    delete mySide;
    delete otherSide; 
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 * 
 * Plays an othello move based on prioritized availability.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
    // Play the opponent's move and generate a new move to fill in. 
    board->doMove(opponentsMove, *otherSide);
    Move *playMove = new Move(-1, -1);
    
    
    // For the following for loops, check the availability of the moves
    // and then play the first one that is available. 
    
    // Check the corners
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 2; j++) {
            playMove->setX(7*i);
            playMove->setY(7*j);
            if (board->checkMove(playMove, *mySide))
            {
                board->doMove(playMove, *mySide);
                return playMove;
            }
        }
    }
    
    // Check the sides not next to corners
    for (int i = 0; i < 2; i++) {
        for (int j = 2; j < 6; j++) {
            playMove->setX(7*i);
            playMove->setY(j);
            if (board->checkMove(playMove, *mySide))
            {
                board->doMove(playMove, *mySide);
                return playMove;
            }
            
            playMove->setX(j);
            playMove->setY(7*i);
            if (board->checkMove(playMove, *mySide))
            {
                board->doMove(playMove, *mySide);
                return playMove;
            }
        }
    }
    
    // Check the corners two tiers in
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 2; j++) {
            playMove->setX(3*i + 2);
            playMove->setY(3*j + 2);
            if (board->checkMove(playMove, *mySide))
            {
                board->doMove(playMove, *mySide);
                return playMove;
            }
        }
    }
    
    // Check the sides two tiers in
    for (int i = 0; i < 2; i++) {
        for (int j = 3; j < 5; j++) {
            playMove->setX(3*i + 2);
            playMove->setY(j);
            if (board->checkMove(playMove, *mySide))
            {
                board->doMove(playMove, *mySide);
                return playMove;
            }
            
            playMove->setX(j);
            playMove->setY(3*i + 2);
            if (board->checkMove(playMove, *mySide))
            {
                board->doMove(playMove, *mySide);
                return playMove;
            }
        }
    }
    
    // Check the sides one tier in which are the closest to the corners
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 2; j++) {
            playMove->setX(3*i + 2);
            playMove->setY(5*j + 1);
            if (board->checkMove(playMove, *mySide))
            {
                board->doMove(playMove, *mySide);
                return playMove;
            }
            
            playMove->setX(5*j + 1);
            playMove->setY(3*i + 2);
            if (board->checkMove(playMove, *mySide))
            {
                board->doMove(playMove, *mySide);
                return playMove;
            }
        }
    }
    
    // Check the remaining sides one tier in
    for (int i = 0; i < 2; i++) {
        for (int j = 3; j < 5; j++) {
            playMove->setX(5*i + 1);
            playMove->setY(j);
            if (board->checkMove(playMove, *mySide))
            {
                board->doMove(playMove, *mySide);
                return playMove;
            }
            
            playMove->setX(j);
            playMove->setY(5*i + 1);
            if (board->checkMove(playMove, *mySide))
            {
                board->doMove(playMove, *mySide);
                return playMove;
            }
        }
    }
    
    // Check the sides next to the corners
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 2; j++) {
            playMove->setX(7*i);
            playMove->setY(5*j + 1);
            if (board->checkMove(playMove, *mySide))
            {
                board->doMove(playMove, *mySide);
                return playMove;
            }
            
            playMove->setX(5*j + 1);
            playMove->setY(7*i);
            if (board->checkMove(playMove, *mySide))
            {
                board->doMove(playMove, *mySide);
                return playMove;
            }
        }
    }
    
    // Check the corners one tier in
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 2; j++) {
            playMove->setX(5*i + 1);
            playMove->setY(5*j + 1);
            if (board->checkMove(playMove, *mySide))
            {
                board->doMove(playMove, *mySide);
                return playMove;
            }
        }
    }
    
    // Having checked all of the moves, none are valid, so pass the turn
    return NULL;
}
